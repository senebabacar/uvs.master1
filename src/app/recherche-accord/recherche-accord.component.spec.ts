import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RechercheAccordComponent } from './recherche-accord.component';

describe('RechercheAccordComponent', () => {
  let component: RechercheAccordComponent;
  let fixture: ComponentFixture<RechercheAccordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechercheAccordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RechercheAccordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
