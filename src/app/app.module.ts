import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApplicationDeGestionDesAccordsDeCooperationComponent } from './application-de-gestion-des-accords-de-cooperation/application-de-gestion-des-accords-de-cooperation.component';
import { RoutingModule } from './routing/routing.module';
import { AccueilComponent } from './components/accueil/accueil.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AjoutezAccordComponent } from './ajoutez-accord/ajoutez-accord.component';
import { RechercheAccordComponent } from './recherche-accord/recherche-accord.component';
import { AccordsComponent } from './accords/accords.component';

@NgModule({
  declarations: [
    AppComponent,
    ApplicationDeGestionDesAccordsDeCooperationComponent,
    AccueilComponent,
    HomeComponent,
    NavMenuComponent,
    AjoutezAccordComponent,
    RechercheAccordComponent,
    AccordsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
