import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutezAccordComponent } from './ajoutez-accord.component';

describe('AjoutezAccordComponent', () => {
  let component: AjoutezAccordComponent;
  let fixture: ComponentFixture<AjoutezAccordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutezAccordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutezAccordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
