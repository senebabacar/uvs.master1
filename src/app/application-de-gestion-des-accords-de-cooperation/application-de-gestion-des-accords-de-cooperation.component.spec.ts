import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationDeGestionDesAccordsDeCooperationComponent } from './application-de-gestion-des-accords-de-cooperation.component';

describe('ApplicationDeGestionDesAccordsDeCooperationComponent', () => {
  let component: ApplicationDeGestionDesAccordsDeCooperationComponent;
  let fixture: ComponentFixture<ApplicationDeGestionDesAccordsDeCooperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationDeGestionDesAccordsDeCooperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationDeGestionDesAccordsDeCooperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
